import Vue from 'vue'
import VueConfirmDialog from 'vue-confirm-dialog' 
import VueRouter from 'vue-router'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faList, faBars, faHome, faUsers, faUser, faShoppingCart, faSearch, faTrash, faRecycle, faCartPlus, faMoneyBill } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faFacebook } from '@fortawesome/free-brands-svg-icons'

library.add(faBars, faList,faHome, faUsers, faUser, faShoppingCart, faSearch, faTrash, faRecycle, faCartPlus, faMoneyBill,faFacebook )
Vue.use(VueRouter)
Vue.use(VueConfirmDialog)

import App from './views/App'
import Home from './views/Home'
import Login from './views/Login'
import Register from './views/Register'
import SingleProduct from './views/SingleProduct'
import Checkout from './views/Checkout'
import Checkout2 from './views/Checkout2'
import Confirmation from './views/Confirmation'
import UserBoard from './views/UserBoard'
import Admin from './views/Admin'
import Cart from './views/Cart'
import Footer from './views/include/Footer'
import Category from './views/include/Category'
import Banner from './views/include/Banner'

Vue.component('category', Category)
Vue.component('banner', Banner)
Vue.component('bottom', Footer)
Vue.component('vue-confirm-dialog',VueConfirmDialog)
Vue.component('font-awesome-icon', FontAwesomeIcon)

//Vue.use(Vuex);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/register',
            name: 'register',
            component: Register
        },
        {
            path: '/products/:id',
            name: 'single-products',
            component: SingleProduct
        },
        {
            path: '/confirmation',
            name: 'confirmation',
            component: Confirmation
        },
        {
            path: '/checkout',
            name: 'checkout',
            component: Checkout,
            props: (route) => ({pid : route.query.pid})
        },
        {
            path: '/checkout2',
            name: 'checkout2',
            component: Checkout2,
        },
        {
            path: '/cart',
            name: 'cart',
            component: Cart,
            props: (route) => ({pid : route.query.pid})
        },
        {
            path: '/dashboard',
            name: 'userboard',
            component: UserBoard,
            meta: { 
                requiresAuth: true,
                is_user : true
            }
        },
        {
            path: '/admin/:page',
            name: 'admin-pages',
            component: Admin,
            meta: { 
                requiresAuth: true,
                is_admin : true
            }
        },
        {
            path: '/admin',
            name: 'admin',
            component: Admin,
            meta: { 
                requiresAuth: true,
                is_admin : true
            }
        },
        {
            path: '/facebook',
            name: 'facebook',
        },
    ],
})
router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem('jwt') == null) {
      next({
        path: '/login',
        params: { nextUrl: to.fullPath }
      })
    } else {
      let user = JSON.parse(localStorage.getItem('user'))
      if(to.matched.some(record => record.meta.is_admin)) {
        if(user.is_admin == 1){
            next()
        }
        else{
            next({ name: 'userboard'})
        }
      }
      else if(to.matched.some(record => record.meta.is_user)) {
        if(user.is_admin == 0){
            next()
        }
        else{
            next({ name: 'admin'})
        }
      }
      next()
    }
  } else {
    next() 
  }
})

const app = new Vue({
    el: '#app',
    components: { App },
    router,
});