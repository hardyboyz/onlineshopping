
<head>
    <meta name="csrf-token" content="{{csrf_token()}}">
</head>
    <script src="{{ mix('js/bootstrap.js') }}"></script>
    <script>
        var token = '{{ $token }}'
    //console.log(token);
        var user = '{{ $user }}';
        data_user = JSON.parse(user.replace(/&quot;/g,'"'));
        //console.log(data_user);
        localStorage.setItem('user',JSON.stringify(data_user))
        localStorage.setItem('jwt',token)
        
        if (localStorage.getItem('jwt') != null){
            self.isLoggedin = true;
            window.opener.location.reload();
            window.close();
        }
    </script>
