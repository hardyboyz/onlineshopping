<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$products = [
    		[
				'product_id'	=> 'C001',
    			'name' => 'Samsung Galaxy S9',
				'description' => 'A brand new, sealed Lilac Purple Verizon Global Unlocked Galaxy S9 by Samsung.',
				'image' => 'https://i.ebayimg.com/00/s/ODY0WDgwMA==/z/9S4AAOSwMZRanqb7/$_35.JPG?set_id=89040003C1',
				'price' => 698.88
    		],
    		[
				'product_id'	=> 'C002',
    			'name' => 'Apple iPhone X',
				'description' => 'iPhone X with iOS 11.',
				'image' => 'https://i.ebayimg.com/00/s/MTYwMFg5OTU=/z/9UAAAOSwFyhaFXZJ/$_35.JPG?set_id=89040003C1',
				'price' => 983.00
    		],
    		[
				'product_id'	=> 'C003',
    			'name' => 'Google Pixel 2 XL',
				'description' => 'New condition,eBay Money back guarantee',
				'image' => 'https://i.ebayimg.com/00/s/MTYwMFg4MzA=/z/G2YAAOSwUJlZ4yQd/$_35.JPG?set_id=89040003C1',
				'price' => 675.00
    		],
    		[
				'product_id'	=> 'C004',
    			'name' => 'LG V10 H900',
				'description' => 'Technology GSM. Protection Gorilla Glass 4.',
				'image' => 'https://i.ebayimg.com/00/s/NjQxWDQyNA==/z/VDoAAOSwgk1XF2oo/$_35.JPG?set_id=89040003C1',
				'price' => 159.99
			],
			[
				'product_id'	=> 'C005',
    			'name' => 'Huawei Elate',
				'description' => 'New Sealed Huawei Elate Smartphone.',
				'image' => 'https://ssli.ebayimg.com/images/g/aJ0AAOSw7zlaldY2/s-l640.jpg',
				'price' => 68.00
			],
			[
				'product_id'	=> 'C006',
				'name' => 'HTC One M10',
				'description' => 'The device is in good cosmetic condition.',
				'image' => 'https://i.ebayimg.com/images/g/u-kAAOSw9p9aXNyf/s-l500.jpg',
				'price' => 129.99
			]
    	];

    	foreach ($products as $product) {
    		Product::create($product);
    	}

    }
}
