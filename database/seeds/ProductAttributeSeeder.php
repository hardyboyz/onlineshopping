<?php

use Illuminate\Database\Seeder;
use App\ProductAttribute;

class ProductAttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$attribute = [
    		[
				'product_id'	=> '1',
    			'weight' => '10KG',
				'color' => 'GREEN',
				'price' => 698,
				'cost' => 698
    		],
    		[
				'product_id'	=> '1',
    			'weight' => '10KG',
				'color' => 'BLUE',
				'price' => 698,
				'cost' => 698
    		],
    		[
				'product_id'	=> '2',
    			'weight' => '10KG',
				'color' => 'PURPLE',
				'price' => 698,
				'cost' => 698
    		],
    		[
				'product_id'	=> '2',
    			'weight' => '0.5KG',
				'color' => 'BLACK',
				'price' => 698,
				'cost' => 698
    		],
    		[
				'product_id'	=> '3',
    			'weight' => '10KG',
				'color' => 'PURPLE',
				'price' => 698,
				'cost' => 698
    		],
    		[
				'product_id'	=> '3',
    			'weight' => '10KG',
				'color' => 'ORANGE',
				'price' => 698,
				'cost' => 698
    		],
			[
				'product_id'	=> '4',
    			'weight' => '10KG',
				'color' => 'GREEN',
				'price' => 698,
				'cost' => 698
    		],
    		[
				'product_id'	=> '4',
    			'weight' => '10KG',
				'color' => 'BLUE',
				'price' => 698,
				'cost' => 698
			],
			[
				'product_id'	=> '5',
    			'weight' => '10KG',
				'color' => 'GREEN',
				'price' => 698,
				'cost' => 698
    		],
    		[
				'product_id'	=> '5',
    			'weight' => '10KG',
				'color' => 'BLUE',
				'price' => 698,
				'cost' => 698
			],
			[
				'product_id'	=> '6',
    			'weight' => '10KG',
				'color' => 'GREEN',
				'price' => 698,
				'cost' => 698
    		],
    		[
				'product_id'	=> '6',
    			'weight' => '10KG',
				'color' => 'BLUE',
				'price' => 698,
				'cost' => 698
    		],
    	];

    	foreach ($attribute as $attr) {
    		ProductAttribute::create($attr);
    	}

    }
}
