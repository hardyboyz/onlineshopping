Laravel Online Shopping,
Developed by : Hardiansyah.
Created for technical testing.

1. Setting database in .env
2. composer install *composer dump-autoload if needed after it
3. php artisan migrate
4. php artisan db:seed
5. php artisan passport:install * with --force if needed to
6. npm install
7. npm run dev
8. php artisan serve