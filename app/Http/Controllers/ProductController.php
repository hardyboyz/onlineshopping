<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(){
        return response()->json(Product::all(),200);
    }
    
    public function store(Request $request){
        $product = Product::create([
            'product_id'    => $request->product_id,
            'name'          => $request->name,
            'description'   => $request->description,
            'units'         => $request->units,
            'price'         => $request->price,
            'image'         => $request->image
        ]);
        
        return response()->json([
            'status' => (bool) $product,
            'data'   => $product,
            'message' => $product ? 'Product Created!' : 'Error Creating Product'
        ]);
    }
    
    public function show(Product $product){
        return response()->json($product::with(['attribute'])->find($product->id),200); 
    }

    public function uploadFile(Request $request){
        if($request->hasFile('image')){
            $name = time()."_".$request->file('image')->getClientOriginalName();
            $request->file('image')->move(public_path('images'), $name);
        }
        return response()->json(asset("images/$name"),201);
    }

    public function update(Request $request, Product $product){
        $status = $product->update(
            $request->only(['product_id','name', 'description', 'units', 'price', 'image'])
        );
        
        return response()->json([
            'status' => $status,
            'message' => $status ? 'Product Updated!' : 'Error Updating Product'
        ]);
    }
    
    public function updateUnits(Request $request, Product $product){
        $product->units = $product->units + $request->get('units');
        $status = $product->save();
        
        return response()->json([
            'status' => $status,
            'message' => $status ? 'Units Added!' : 'Error Adding Product Units'
        ]);
    }

    public function destroy(Product $product){
        $status = $product->delete();
        
        return response()->json([
            'status' => $status,
            'message' => $status ? 'Product Deleted!' : 'Error Deleting Product'
        ]);
    }

    public function search($key = '-', $price = '-'){
        $query = Product::with(['attribute']);
        if($key != '-'){
            $query->where('name','LIKE', '%'.$key.'%');
        }
        
        if($price != '-'){
            $query->where('price','<', $price);
        }

        $search = $query->get();

        return response()->json($search,200); 
    }
}
