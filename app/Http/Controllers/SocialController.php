<?php
namespace App\Http\Controllers;

use DB;
use Socialite;
use Auth;
use App\User;
use View;

class SocialController extends Controller
{
    public function redirect($provider){
        return Socialite::driver($provider)->redirect();
        // $redirect = Socialite::driver($provider)->redirect();
        // $provider = $provider;
                     
        // return View::make('callback')->with(compact('provider','redirect'));

    }

    public function Callback($provider){
        $userSocial =   Socialite::driver($provider)->stateless()->user();
        $users      =   User::where(['email' => $userSocial->getEmail()])->first();
        
        $user = [];
        $token = '';
        if($users){
                
                if (auth()->loginUsingId($users->id)) {
                    $token = Auth::user()->createToken('onlineshop')->accessToken;
                    $user = Auth::user();
                }                
            
        }else{
                $user = User::create([
                    'name'          => $userSocial->getName(),
                    'email'         => $userSocial->getEmail(),
                    //'image'         => $userSocial->getAvatar(),
                    'provider_id'   => $userSocial->getId(),
                    'provider'      => $provider,
                ]);

                $users = User::where(['email' => $userSocial->getEmail()])->first();
                if (auth()->loginUsingId($users->id)) {
                    $token = Auth::user()->createToken('onlineshop')->accessToken;
                    $user = Auth::user();
                }  
        }

            return View('callback',compact('token','user'));

    }
}
