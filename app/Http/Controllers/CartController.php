<?php

namespace App\Http\Controllers;

use App\Product;
use Auth;
use Illuminate\Http\Request;
use Session;

class CartController extends Controller
{
    public function index(){
        return response()->json(Order::with(['product'])->get(),200);
    }

    public function cart(Order $order){
        return response()->json(Order::with(['product'])->where('is_cart',1)->get(),200);
    }

    public function addToCart(Request $request, $id){
        $product = Product::find($id);
        $cart = Session::get('cart');
        $cart[$product->id] = array(
            "id" => $product->id,
            "name" => $product->name,
            "description" => $product->price,
            "image" => $product->image,
            "quantity" => 1,
        );

        Session::put('cart', $cart);
        
    }

    public function updateCart(Request $cartdata){
        $cart = Session::get('cart');

        foreach ($cartdata->all() as $id => $val) 
        {
            if ($val > 0) {
                $cart[$id]['qty'] += $val;
            } else {
                unset($cart[$id]);
            }
        }
        Session::put('cart', $cart);
        return redirect()->back();
    }

}