<?php

namespace App\Http\Controllers;

use App\Order;
use Auth;
use Illuminate\Http\Request;
use DB;

class OrderController extends Controller
{


    public function index(){
        return response()->json(Order::with(['product'])->get(),200);
    }
        
    public function deliverOrder(Order $order){
        $order->is_delivered = true;
        $status = $order->save();
        
        return response()->json([
            'status'    => $status,
            'data'      => $order,
            'message'   => $status ? 'Order Delivered!' : 'Error Delivering Order'
        ]);
    }
    
    public function store(Request $request){
        DB::beginTransaction();

        $user = auth('api')->user();
        //print_r($user->id);exit;
        $user_id = $user == null ? 0 : $user->id;

        try {
            $order_code = strtoupper(uniqid());

            if(is_array($request->products)){
                foreach($request->products as $p){

                    $order[] = [
                        'order_code'    => $order_code,
                        'product_id'    => $p['id'],
                        'attribute_id'  => $p['product_attribute']['id'],
                        'user_id'       => $user_id,
                        'quantity'      => $p['quantity'],
                        'address'       => $request->address,
                        'total'         => $p['rowTotal'],
                        'created_at'    => now(),
                        'updated_at'    => now(),
                    ];
                }
            }else{
                $order = [
                    'order_code'    => $order_code,
                    'product_id'    => $request->product,
                    'attribute_id'  => $request->product_attribute,
                    'user_id'       => $user_id,
                    'quantity'      => $request->quantity,
                    'address'       => $request->address,
                    'total'         => $request->total,
                    'created_at'    => now(),
                    'updated_at'    => now(),
                ];
            }

            DB::table('orders')->insert($order);
            DB::commit();

            return response()->json([
                'status' => true,
                'data'   => $order_code,
                'message' => 'Order Created!'
            ]);

        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'status' => false,
                'data'   => [],
                'message' => $e->getMessage()
            ]);
        }
        
    }

    public function show(Order $order){
        return response()->json($order,200);
    }
    
    public function update(Request $request, Order $order){
        $status = $order->update(
            $request->only(['quantity'])
        );
        
        return response()->json([
            'status' => $status,
            'message' => $status ? 'Order Updated!' : 'Error Updating Order'
        ]);
    }

    public function destroy(Order $order){
        $status = $order->delete();
        
        return response()->json([
            'status' => $status,
            'message' => $status ? 'Order Deleted!' : 'Error Deleting Order'
        ]);
    }
}