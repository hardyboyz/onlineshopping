<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $table ='products';
    
    protected $fillable = [
        'name', 'price', 'units', 'description', 'image', 'product_id'
    ];
    
    public function orders(){
        return $this->hasMany(Order::class);
    }

    public function attribute(){
        return $this->hasMany(ProductAttribute::class);
    }
}