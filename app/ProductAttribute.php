<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductAttribute extends Model
{
    
    protected $table ='product_attributes';
    protected $fillable = [
        'attribute_name'
    ];
    public $timestamps = false;

    public function product(){
        return $this->belongsTo(Product::class);
    }

}